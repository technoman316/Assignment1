package Assignment1;
import java.util.Scanner;
public class Assignment1 {
	/**
	 * A program to display how old you will be in a certain year
	 * @author cjp2790
	 * Date: 9/5/14
	 * Algorithm:
	 * 1. Take in a person's birth year
	 * 2. Take in the person's age
	 * 3. Calculate the year
	 * 4. Display the year to the screen
	**/

	public static void main(String[] args) {
		int n1, n2;
		
		//Take in the birth year
		System.out.println("Enter the year you were born:");
		Scanner birthyear = new Scanner(System.in);
		n1 = birthyear.nextInt();
		
		//Take in the end year
		System.out.println("Enter an age to determine the year you will be that age:");
		Scanner age = new Scanner(System.in);
		n2 = age.nextInt();
		
		//Calculate age and display it to the screen
		System.out.println("The year will be:");
		System.out.println(n1 + n2);
	}
}